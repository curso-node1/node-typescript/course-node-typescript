import obtenerPromedioDeDosNumeros from "./statistics";
import Persona from "./dto/persona";
import { NUMERO_ANIOS_MAYOR_EDAD } from "./constants";

console.log('El promedio de dos números: ', obtenerPromedioDeDosNumeros(10, 20));


let persona: Persona = new Persona();
persona.nombre = 'Pepito';
persona.edad = 10;

if (persona.edad >= NUMERO_ANIOS_MAYOR_EDAD) { // if (persona.edad >= 18) {
  console.log('Es mayor de edad');
} else {
  console.log('No es mayor de edad');
}
