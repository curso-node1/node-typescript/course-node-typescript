class Persona {
  /** Este es el nombre que le pusieron sus padres */
  nombre: string;
  /** Esta es la edad que tiene actualmente */
  edad: number;
}

export default Persona;