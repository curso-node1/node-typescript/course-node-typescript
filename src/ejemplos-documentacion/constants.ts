/** URL para conectarme a mi base de datos local en mongo */
export const URL_CONEXION_MONGO = 'mongodb:;//admin:SuperSecretPassword@localhost:27017/admin';

/** Esta es la edad en la que una personas es mayor de edad en México */
export const NUMERO_ANIOS_MAYOR_EDAD = 18;
