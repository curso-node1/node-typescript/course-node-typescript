/**
 * Regresa el promedio de dos números.
 *
 * @param numero1 - El primer número de entrada.
 * @param numero 2 - El segundo número de entrada.
 * @returns El promedio de `numero1` y `numero2`
 *
 */
function obtenerPromedioDeDosNumeros(numero1: number, numero2: number): number {
  return (numero1 + numero2) / 2.0;
}

export default obtenerPromedioDeDosNumeros;
