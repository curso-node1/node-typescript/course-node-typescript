// Hay dos formas de declarar una función

// función Tradicional
function suma(n1, n2) {
  return n1 + n2;
}

// Arrow function
const sumaArrowFunction = (n1, n2) => {
  return n1 + n2;
};

console.log('suma como funcion normal: ', suma(1, 2));
console.log('suma función flecha (arrow function): ', sumaArrowFunction(1, 2));
