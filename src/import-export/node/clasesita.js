class ClasesitaBonita {
  /**
   * retorna un saludo muy particular de clasesita bonita
   */
  static saludo() {
    return 'Hola soy Clasesita Mucho gusto!';
  }
}

module.exports = ClasesitaBonita;