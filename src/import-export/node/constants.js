const SALUDO = 'Hola bro! :D';

/**
 * 
 * @param nombre - El nombre al que quieras mandar saludo 
 * @returns Un saludo con tú nombre
 */
function saludoConMiNombre(nombre) {
  return `Hola ${nombre}! :D`; // tambien se puede return 'Hola '+ nombre;
}

module.exports = {
  SALUDO,
  saludoConMiNombre,
};
