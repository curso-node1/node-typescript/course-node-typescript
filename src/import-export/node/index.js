const { SALUDO, saludoConMiNombre } = require('./constants');
const Clasesita = require('./clasesita');

console.log('Constante SALUDO: ', SALUDO);
console.log('Función saludoConMiNombre: ', saludoConMiNombre('Isaac'));
console.log('Clasesita: ', Clasesita.saludo());

