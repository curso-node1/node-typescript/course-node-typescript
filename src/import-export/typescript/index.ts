import { SALUDO, saludoConMiNombre } from './constants';
import Clasesita from './clasesita';

console.log('Constante SALUDO: ', SALUDO);
console.log('Función saludoConMiNombre: ', saludoConMiNombre('Isaac'));
console.log('Clasesita: ', Clasesita.saludo());

