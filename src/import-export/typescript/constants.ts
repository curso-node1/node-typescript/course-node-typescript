const SALUDO = 'Hola bro!';
/**
 * 
 * @param nombre - El nombre al que quieras mandar saludo 
 * @returns Un saludo con el nombre que pasaste por parametro
 */
function saludoConMiNombre(nombre: string) {
  return `Hola ${nombre}`; // tambien se puede return 'Hola '+ nombre;
}

export {
  SALUDO,
  saludoConMiNombre,
};