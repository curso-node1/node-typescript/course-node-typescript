// Hay tres formas de definir variables var, let y const

// declarar variable con var
var mejorGuerrero = 'Vegeta';

function elMejorGuerreroZSegunBulma() {
  let mejorGuerrero = 'Goku'; // Esta variable solo esta disponible dentro de la función
  console.log('Bulma dice: el Mejor guerrero  es ', mejorGuerrero);
}

elMejorGuerreroZSegunBulma();
console.log('Node dice: el mejor guerrero es: ', mejorGuerrero);

// declarar variable con const
const EL_MEJOR_ANIME = 'Dragon Ball Z';

// EL_MEJOR_ANIME = 'One Piece'; // error TS2588: Cannot assign to 'elMejorAnime' because it is a constant.
