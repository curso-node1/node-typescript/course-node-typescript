var numero = 0; // number

var cadena = 'Hola amiguitos'; // string

var valorBoleano = false; // boolean

var valorIndefinido; // undefined

var valorNulo = null; // null

var array = [1, 2]; // object

var objeto = { id: 1, nombre: 'Objetito bonito' }; // object

function funcion() { } // function

var arregloDeVariables = [
  numero,
  valorBoleano,
  cadena,
  objeto,
  valorIndefinido,
  valorNulo,
  array,
  funcion,
];


arregloDeVariables.forEach((variable) => {
  imprimirTipoDeDato(variable);
});

function imprimirTipoDeDato(variable) {
  console.log(`${variable}: ${typeof variable}`);
};
