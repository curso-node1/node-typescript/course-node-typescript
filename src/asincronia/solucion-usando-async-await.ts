import Dog from "./dto/dog.dto";
import { buscarPerrosEnLaBaseDeDatos } from "./perros.service";

let perrosEncontrados: Dog[];

const runSolucionUsandoAsyncAwait = async () => {
  try {
    perrosEncontrados = await buscarPerrosEnLaBaseDeDatos();
    console.log('Se encontraron los perros?: ', perrosEncontrados);
  } catch (error) {
    console.log(error);
  } finally {
    console.log('Esto siempre se ejecuta, haya o no haya error');
  }
};

runSolucionUsandoAsyncAwait();