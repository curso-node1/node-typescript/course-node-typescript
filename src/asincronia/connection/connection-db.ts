import { Client } from 'pg'; // https://node-postgres.com/

const clientPostgres = new Client({
  user: 'postgres',
  host: 'localhost',
  database: 'db',
  password: 'SuperSecretPassword',
  port: 5433,
});

clientPostgres.connect();


export default clientPostgres;