import Dog from "./dto/dog.dto";
import { buscarPerrosEnLaBaseDeDatos } from "./perros.service";

let perrosEncontrados: Dog[];

const runSolucionUsandoAsyncAwait = async () => {
    buscarPerrosEnLaBaseDeDatos()
        .then((responsePromise: Array<Dog>) => {
            perrosEncontrados = responsePromise;
            console.log('Se encontraron los perros?: ', perrosEncontrados);
        }).catch((error) => {
            console.log(error);
        }).finally(() => {
            console.log('Esto siempre se ejecuta, haya o no haya error');
        });
};

runSolucionUsandoAsyncAwait();
