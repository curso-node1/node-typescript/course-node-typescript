import clientPostgres from "./connection/connection-db";
import { QUERY_BUSCAR_PERROS } from "./constants";
import Dog from "./dto/dog.dto";

/**
 * Busca los perritos en postgres usando las promesas en node
 * @returns perritosEncontrados
 * @author Isaac López
 */
export function buscarPerrosEnLaBaseDeDatos(): Promise<Dog[]> {
    return new Promise((resolve, rejects) => {
        clientPostgres.query(QUERY_BUSCAR_PERROS, (error, res) => {
            if (!error) {
                clientPostgres.query(QUERY_BUSCAR_PERROS, (error, res) => {
                    resolve(res.rows);
                });

            } else {
                rejects("Hubo un error al buscar los perros en la base de datos :'v");
            }
        });
    });
}
