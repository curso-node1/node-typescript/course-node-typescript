class Dog {
  /** Llave primaria autoincremental del perrito */
  id: number;
  /** Nombre del perrito */
  name: string;
}

export default Dog;