import clientPostgres from './connection/connection-db';
import { QUERY_BUSCAR_PERROS } from './constants';
import Dog from './dto/dog.dto';

let perrosEncontrados: Dog[];

const run = () => {
  clientPostgres.query(QUERY_BUSCAR_PERROS, (error, res) => {
    if (!error) {
      perrosEncontrados = res.rows;
      // console.log('Se encontraron los perros despues de: ', perrosEncontrados);
      for (let perro of perrosEncontrados) {
        clientPostgres.query(`select * from catalogo_perros where idPerrro=${perro.id}`, (error, res) => {
        });
      }
    } else {
      console.log("Hubo un error :'v ");
      perrosEncontrados = [];
    }
  });

};

run();
