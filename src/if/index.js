// Algo curioso con los if

var numero = 0; // number

var cadena = ''; // string

var valorBoleano = false; // boolean

var valorIndefinido;

var valorNulo = null;

var arregloVacio = []; // object

var objetoVacio = {}; // object

function funcionVacia() { }

if (numero) {
  console.log('Entró con el número');
} else {
  console.log('No Entró con el número');
}

if (cadena) {
  console.log('Entró con la cadena');
} else {
  console.log('No entró con la cadena');
}

if (valorBoleano) {
  console.log('Entró con el valorBoleano');
} else {
  console.log('No entró con el valorBoleano');
}

if (valorNulo) {
  console.log('Entró con el valorNulo');
} else {
  console.log('No entró con el valorNulo');
}

if (valorIndefinido) {
  console.log('Entró con el valorIndefinido');
} else {
  console.log('No entró con el valorIndefinido');
}

if (arregloVacio) {
  console.log('Entró con el arregloVacio');
} else {
  console.log('No entró con el arregloVacio');
}

if (objetoVacio) {
  console.log('Entró con el objetoVacio');
} else {
  console.log('No entró con el objetoVacio');
}

if (funcionVacia) {
  console.log('Entró con la funcionVacia');
} else {
  console.log('No entró con la funcionVacia');
}