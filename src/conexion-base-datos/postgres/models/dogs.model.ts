import { Model, DataTypes } from "sequelize";
import sequelize from "../conexion-postgres";

class DogsModel extends Model {
  /** Este es el id autoincremental que se guarda en postgres */
  public id: number;
  /** Este es el nombre del perrito */
  public name: string;
}

DogsModel.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  name: {
    type: new DataTypes.STRING,
    allowNull: false,
  }
}, {
  tableName: 'dogs',
  sequelize: sequelize,
  timestamps: false
});


export {
  DogsModel,
};
