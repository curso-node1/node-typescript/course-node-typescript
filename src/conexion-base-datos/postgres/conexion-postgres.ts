// https://sequelize.org/v5/manual/getting-started.html
import { Sequelize } from 'sequelize';

const sequelize = new Sequelize({
  dialect: 'postgres',
  username: 'postgres',
  host: 'localhost',
  database: 'db',
  password: 'SuperSecretPassword',
  port: 5433
});

export default sequelize;
