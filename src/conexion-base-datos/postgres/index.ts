import { DogsModel } from './models/dogs.model';

const main = async () => {
  try {
    const perritosEncontrados = await DogsModel.findAll(); // o sequelize.query("SELECT * FROM dogs", { type: QueryTypes.SELECT });
    for (const perrito of perritosEncontrados) {
      console.log(perrito.name);
    }
  } catch (error) {
    console.log('Hubo un error en la base de datos ', error);
  }
};

main();