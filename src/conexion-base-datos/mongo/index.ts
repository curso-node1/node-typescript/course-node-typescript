// https://mongoosejs.com/docs/index.html
import { db } from './conexion-mongoose';
import DogsModel from './models/dogs.model';

const main = async () => {
  try {
    const perritosEncontrados = await DogsModel.find();
    for (const perrito of perritosEncontrados) {
      console.log(perrito.name);
    }
  } catch (error) {
    console.log('Hubo un error en la base de datos ', error);
  } finally {
    db.close();
  }
};

main();