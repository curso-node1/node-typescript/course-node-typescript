import { Document, Schema, model } from 'mongoose';
//Mongoose.pluralize(null);

export interface DogsModel extends Document {
  /** Este es el nombre del perrito */
  name: string;
}

const dogsSchema = new Schema({
  name: String
}, { collection: 'dogs' }
);


export default model<DogsModel>('dogs', dogsSchema);
